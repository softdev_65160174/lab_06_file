/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.oxoop;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nobpharat
 */
public class Friends implements Serializable{
    private int id ;
    private String name;
    private int age;
    private String tel;
    private static int lastId;

    public Friends(String name, int age, String tel) {
        this.id = lastId++;
        this.name = name;
        this.age = age;
        this.tel = tel;
    }

    @Override
    public String toString() {
        return "Friends{" + "id=" + id + ", name=" + name + ", age=" + age + ", tel=" + tel + '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws Exception {
        if(age <0){
            throw  new Exception();
        }
        this.age = age;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public static int getLastId() {
        return lastId;
    }

    public static void setLastId(int lastId) {
        Friends.lastId = lastId;
    }
    
    
    public static void main(String[] args) {
        try {
            Friends f1 = new Friends("Pom", 70,"08111111111");
            Friends f2 = new Friends("Tu", 69, "0000000000");
            System.out.println(f1.toString());
            System.out.println(f2.toString());
            f1.setAge(-1);
        } catch (Exception ex) {
            System.out.println("Shit");
        }
    }
}
